# Entrega #1
## Prueba Diagnostica
### Sistemas De Inteligencia Artificial 

![Ejercicios solicitados por el docente](./img/Ejercicios.png)

## Resultados 

### Apartado A
![Punto 1](./img/Imagen_punto_1.jpg)
![Punto 2](./img/Imagen_punto_2.jpg)
![Punto 3](./img/Imagen_punto_3.jpg)

### Apartado B

![Punto A codingame](./img/Punto_4A.png)
![Punto B codingame](./img/Punto_4B.png)
![Punto C codingame](./img/Punto_4C.png)